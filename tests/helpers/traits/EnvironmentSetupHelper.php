<?php

namespace Tests\Helpers\Traits;

use SmartUber\Core\Exceptions\UnknownInterfaceException;
use SmartUber\Core\Exceptions\UnknownModelException;

trait EnvironmentSetupHelper
{
    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate', ['--database' => 'smart-uber']);
        if (class_exists(\SmartUber\Payment\Models\Payment::class)) {
            $this->loadMigrationsFrom(realpath(__DIR__.'/../../../vendor/smart-uber/payment/migrations'));
        }
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../../migrations'));
        $this->withFactories(__DIR__.'/../../../factories');
    }

    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'smart-uber');
        $app['config']->set('database.connections.smart-uber', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function getPackageProviders($app)
    {
        $packageProviders = [];

        if (class_exists(\SmartUber\Payment\Models\Payment::class)) {
            $packageProviders[] = \SmartUber\Payment\Providers\PaymentServiceProvider::class;
        }

        array_push($packageProviders,
            \SmartUber\Core\Providers\CoreServiceProvider::class,
            \Orchestra\Database\ConsoleServiceProvider::class
        );
        
        return $packageProviders;
    }
}