<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Config;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Orchestra\Testbench\TestCase;
use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Helpers\Enums\ProviderRequisitionStatus;
use SmartUber\Core\Helpers\Enums\RateType;
use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Helpers\Enums\TenderStatus;
use SmartUber\Core\MatchMaker;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;

class ProviderTest extends TestCase
{
    use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAcceptCompanyRequisition()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $providerRequisition = $this->getProviderRequisition($requisition)->first();
        $provider->acceptCompanyRequisition($providerRequisition);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(ProviderRequisition::class, $providerRequisition);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(ProviderRequisitionStatus::ACCEPT, $providerRequisition->status);
    }

    public function testRejectCompanyRequisition()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $providerRequisition = $this->getProviderRequisition($requisition)->first();
        $provider->rejectCompanyRequisition($providerRequisition);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(ProviderRequisition::class, $providerRequisition);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(ProviderRequisitionStatus::REJECT, $providerRequisition->status);
    }

    public function testCancelCompanyRequisition()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $providerRequisition = $this->getProviderRequisition($requisition)->first();
        $provider->cancelCompanyRequisition($providerRequisition);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(ProviderRequisition::class, $providerRequisition);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(ProviderRequisitionStatus::CANCEL, $providerRequisition->status);
    }

    public function testPostTender()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->min_price = rand(1, 1000);
        $requisition->max_price = rand(1, 1000);
        $requisition->rate_type = rand(1, 1000);
        $requisition->setStatusAsPost();

        $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
    }

    public function testCloseTender()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->min_price = rand(1, 1000);
        $requisition->max_price = rand(1, 1000);
        $requisition->rate_type = rand(1, 1000);
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $provider->closeTender($tender);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::CLOSE, $tender->status);
    }

    public function testCancelTender()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->min_price = rand(1, 1000);
        $requisition->max_price = rand(1, 1000);
        $requisition->rate_type = rand(1, 1000);
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $provider->cancelTender($tender);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::CANCEL, $tender->status);
    }

    public function testAcceptApplicant()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();

        $applicant = $this->getApplicant($tender)->first();
        $provider->acceptApplicant($applicant);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Applicant::class, $applicant);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertEquals(ApplicantStatus::ACCEPTED_BY_PROVIDER, $applicant->status);
    }

    public function testRejectApplicant()
    {
        $provider = factory(Provider::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();

        $applicant = $this->getApplicant($tender)->first();
        $provider->rejectApplicant($applicant);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Applicant::class, $applicant);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertEquals(ApplicantStatus::REJECTED_BY_PROVIDER, $applicant->status);
    }

    public function testGetTotalTenderByStatus()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisition = $this->getDummyPostedRequisition($company, $branch, $provider);

        for ($i = 0; $i < 10; $i++) {
            $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);
            switch ($i) {
                case 2:
                case 3:
                case 4:
                    $provider->closeTender($tender);
                break;
                case 5:
                case 6:
                case 7:
                    $provider->cancelTender($tender);
                break;
            }
        }

        $totalOpenedTender = $provider->tenders()->where('status', TenderStatus::OPEN)->get();
        $totalClosedTender = $provider->tenders()->where('status', TenderStatus::CLOSE)->get();
        $totalCancelledTender = $provider->tenders()->where('status', TenderStatus::CANCEL)->get();

        $this->assertCount(4, $totalOpenedTender);
        $this->assertCount(3, $totalClosedTender);
        $this->assertCount(3, $totalCancelledTender);
        $this->assertCount(10, $provider->tenders);
    }

    public function testGetTotalApplicationByStatusInTender()
    {
        $faker = Faker::create();
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $members = $provider->members;
        $requisition = $this->getDummyPostedRequisition($company, $branch, $provider);
        $tender = $this->getDummyTender($requisition);

        $members->each(function ($member, $index) use ($tender, $faker) {
            $provider = $member->provider;
            $company = $provider->company;

            Config::set('core_config.auto_accept_applicant', false);

            switch ($index) {
                case 1:
                    Config::set('core_config.auto_accept_applicant', true);
                    $member->applyTender($tender, $faker->dateTime);
                    break;
                case 2:
                case 3:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $member->cancelApplication($applicant);
                    break;
                case 4:
                case 5:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->rejectApplicant($applicant);
                    break;
                case 6:
                case 7:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    break;
                case 8:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    $company->acceptApplicant($applicant);
                    break;
                case 9:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    $company->rejectApplicant($applicant);
                    break;
            }
        });

        $tender = $provider->tenders()->where('id', $tender->id)->first();
        $applicants = $tender->applicants;

        $totalSubmittedApplication = $applicants->where('status', ApplicantStatus::SUBMITTED);
        $totalCancelledApplication = $applicants->where('status', ApplicantStatus::CANCELLED);
        $totalAcceptedByProviderApplication = $applicants->where('status', ApplicantStatus::ACCEPTED_BY_PROVIDER);
        $totalRejectedByProviderApplication = $applicants->where('status', ApplicantStatus::REJECTED_BY_PROVIDER);
        $totalAcceptedByCompanyApplication = $applicants->where('status', ApplicantStatus::ACCEPTED_BY_COMPANY);
        $totalRejectedByCompanyApplication = $applicants->where('status', ApplicantStatus::REJECTED_BY_COMPANY);

        $this->assertCount(9, $applicants);
        $this->assertCount(10, $members);
        $this->assertCount(1, $totalSubmittedApplication);
        $this->assertCount(2, $totalCancelledApplication);
        $this->assertCount(2, $totalAcceptedByProviderApplication);
        $this->assertCount(2, $totalRejectedByProviderApplication);
        $this->assertCount(1, $totalAcceptedByCompanyApplication);
        $this->assertCount(1, $totalRejectedByCompanyApplication);
    }

    // public function testGetTotalProviderRequisitionByStatus()
    // {
    //     $company = $this->getDummyCompanyModel();
    //     $branch = $company->branches()->first();
    //     $provider = $company->providers()->first();
    //     $requisitions = $this->getDummyPostedRequisition($company, $branch, $provider, 10);

    //     $providerRequisitions = $provider->providerRequisitions;
    //     $this->assertCount(10, $providerRequisitions);

    //     $providerRequisitions->each(function ($providerRequisition, $index) use ($provider) {
    //         switch ($index) {
    //             case 0:
    //             case 1:
    //             case 2:
    //                 $provider->acceptCompanyRequisition($providerRequisition);
    //                 break;
    //             case 3:
    //             case 4:
    //             case 5:
    //                 $provider->rejectCompanyRequisition($providerRequisition);
    //                 break;
    //             case 6:
    //             case 7:
    //             case 8:
    //                 $provider->cancelCompanyRequisition($providerRequisition);
    //                 break;
    //         }
    //     });

    //     $totalAcceptedCompanyRequisition =
    //         $provider->providerRequisitions()->whereNotNull('status')->where('status', ProviderRequisitionStatus::ACCEPT)->get();
    //     $totalRejectedCompanyRequisition =
    //         $provider->providerRequisitions()->whereNotNull('status')->where('status', ProviderRequisitionStatus::REJECT)->get();
    //     $totalCancelledCompanyRequisition =
    //         $provider->providerRequisitions()->whereNotNull('status')->where('status', ProviderRequisitionStatus::CANCEL)->get();

    //     $this->assertCount(3, $totalAcceptedCompanyRequisition);
    //     $this->assertCount(3, $totalRejectedCompanyRequisition);
    //     $this->assertCount(3, $totalCancelledCompanyRequisition);
    // }

    // public function testGetUserWhichBelongsToProvider()
    // {
    //     $company = $this->getDummyCompanyModel();
    //     $provider = $company->providers()->first();

    //     // $this->assertInstanceOf(User::class, $provider->user);
    //     $this->assertCount(10, $provider->members);
    //     $this->assertCount(0, $provider->requisitions);
    // }

    public function testGetTotalMember()
    {
        $company = $this->getDummyCompanyModel();
        $provider = $company->providers()->first();

        $this->assertCount(10, $provider->members);
    }

    public function testGetTotalRequisitions()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisitions = $this->getDummyPostedRequisition($company, $branch, $provider, 10);

        $this->assertCount(10, $company->requisitions);
    }

    public function testAutoPushNotificationWhenOpenTender()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisition = $this->getDummyPostedRequisition($company, $branch, $provider);

        Config::set('core_config.auto_push_notification', true);
        $tender = $provider->openTender(
            $requisition,
            RateType::HOURLY,
            rand(1, 100),
            RateType::HOURLY
        );

        // $this->assertCount(10, $tender->invitations);

        // foreach ($tender->invitations as $key => $invitation) {
        //     $this->assertEquals(true, $invitation->has_notified);
        // }
    }

    public function testManualPushNotification()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisition = $this->getDummyPostedRequisition($company, $branch, $provider);


        Config::set('core_config.auto_push_notification', false);
        $tender = $provider->openTender(
            $requisition,
            RateType::HOURLY,
            rand(1, 100),
            RateType::HOURLY
        );
        $provider->pushNotification($tender);
        // $this->assertCount(10, $tender->invitations);

        // foreach ($tender->invitations as $key => $invitation) {
        //     $this->assertEquals(true, $invitation->has_notified);
        // }
    }

    // public function testGetInstanceOfMatchMaker()
    // {
    //     $company = $this->getDummyCompanyModel();
    //     $branch = $company->branches()->first();
    //     $provider = $company->providers()->first();
    //     $matchMaker = $provider->getInstanceOfMatchMaker();
    //     $this->assertInstanceOf(MatchMaker::class, new $matchMaker);
    // }
}
