<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Orchestra\Testbench\TestCase;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTrue()
    {
        $this->assertTrue(true);
    }

    // public function testGetMemberFromProvider()
    // {
    //     $provider = factory(Provider::class)->create();
    //     $members = factory(Member::class, 5)->create([
    //         "provider_id" => $provider->id
    //     ]);
    //     $user = $members->first()->user;
    //     $member = $user->member($provider);

    //     $this->assertCount(5, $members);
    //     $this->assertInstanceOf(Member::class, $member);
    // }

    // public function testGetProviderFromCompany()
    // {
    //     $company = factory(Company::class)->create();
    //     $providers = factory(Provider::class, 5)->create([
    //         "company_id" => $company->id
    //     ]);
    //     $user = $providers->first()->user;
    //     $provider = $user->provider($company);

    //     $this->assertCount(5, $providers);
    //     $this->assertInstanceOf(Provider::class, $provider);
    // }
}
