<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Orchestra\Testbench\TestCase;
use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Requisition\Requisition;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;

class BranchTest extends TestCase
{
    use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOpenRequest()
    {
        $branch = factory(Branch::class)->create();
        $date = Carbon::now();
        $requisition = $branch->openRequest(
            'Branches Requisition',
            $date,
            $date->copy()->addWeeks(1),
            'This is my first requisition'
        );
        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertEquals(RequisitionStatus::OPEN, $requisition->status);
    }

    public function testGetBranchWhichBelongsToCompany()
    {
        $company = $this->getDummyCompanyModel();

        $this->assertInstanceOf(Branch::class, $company->branches()->first());
        $this->assertInstanceOf(Company::class, $company);
    }

    public function testGetBranchIsTotalOpenedRequisition()
    {
        $date = Carbon::now();
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();

        for ($i = 0; $i < 10; $i++) {
            $requisition = $branch->openRequest(
                'Branches Requisition',
                $date,
                $date->copy()->addWeeks(1),
                'This is my first requisition'
            );
        }

        $totalOpenedRequisition = $branch->requisitions()->where('status', RequisitionStatus::OPEN)->get();

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Branch::class, $branch);
        $this->assertCount(10, $branch->requisitions);
        $this->assertCount(10, $totalOpenedRequisition);
    }
}
