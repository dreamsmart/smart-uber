<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Orchestra\Testbench\TestCase;
use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Helpers\Enums\ProviderRequisitionStatus;
use SmartUber\Core\Helpers\Enums\RateType;
use SmartUber\Core\Helpers\Enums\TenderStatus;
use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;

class CompanyTest extends TestCase
{
    use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostMultipleRequest()
    {
        $company = factory(Company::class)->create();
        $providers = factory(Provider::class, 5)->create([
            "company_id" => $company->id
        ]);
        $requisition = $company->postRequest($this->getRequisition(), Carbon::now(), rand(1, 100), rand(1, 100), RateType::HOURLY);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertCount(5, $company->providers);
    }

    public function testPostSingleRequest()
    {
        $company = factory(Company::class)->create();
        $providers = factory(Provider::class)->create([
            "company_id" => $company->id
        ]);
        $requisition = $company->postRequest(
            $this->getRequisition(),
            Carbon::now(),
            rand(1, 100),
            rand(1, 100),
            RateType::HOURLY
        );
        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
    }

    public function testOpenRequest()
    {
        $branch = factory(Branch::class)->create();
        $date = Carbon::now();
        $requisition = $branch->openRequest(
            'Branches Requisition',
            $date,
            $date->copy()->addWeeks(1),
            'This is my first requisition'
        );
        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertEquals(RequisitionStatus::OPEN, $requisition->status);
    }

    public function testCloseRequest()
    {
        $company = factory(Company::class)->create();
        $requisition = $this->getRequisition();
        $company->closeRequest($requisition);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertEquals(RequisitionStatus::CLOSE, $requisition->status);
    }

    public function testAcceptApplicant()
    {
        $company = factory(Company::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();

        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();

        $applicant = $this->getApplicant($tender)->first();
        $company->acceptApplicant($applicant);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertInstanceOf(Applicant::class, $applicant);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertEquals(ApplicantStatus::ACCEPTED_BY_COMPANY, $applicant->status);
    }

    public function testRejectApplicant()
    {
        $company = factory(Company::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();

        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();

        $applicant = $this->getApplicant($tender)->first();
        $company->rejectApplicant($applicant);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertInstanceOf(Applicant::class, $applicant);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertEquals(ApplicantStatus::REJECTED_BY_COMPANY, $applicant->status);
    }

    // public function testMakePayment()
    // {
    //     $tender = $this->getDummyTenderWithApplicant();
    //     $provider = $tender->provider;
    //     $company = $provider->company;
    //     $date = Carbon::now();
    //     $paymentResult = null;

    //     if ($company->hasPaymentDriver()) {
    //         $paymentResult = $company->makePayment($tender, $date, $date);
    //         $this->assertCount(3, $paymentResult['salary']);
    //         $this->assertEquals($paymentResult['commission'], "Payable Commission");
    //     }
    // }

    public function testGetCompanyIsTotalBranchAndProvider()
    {
        $company = $this->getDummyCompanyModel();
        $providers = $company->providers;
        $branch = $company->branches;

        $this->assertInstanceOf(Company::class, $company);
        $this->assertCount(10, $company->branches);
        $this->assertCount(10, $company->providers);
    }

    public function testGetTotalTenderByStatusInRequisition()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisition = $this->getDummyOpenedRequisition($company, $branch);

        for ($i = 0; $i < 10; $i++) {
            $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);
            switch ($i) {
                case 2:
                case 3:
                case 4:
                    $provider->closeTender($tender);
                break;
                case 5:
                case 6:
                case 7:
                    $provider->cancelTender($tender);
                break;
            }
        }

        $requisition  = $company->requisitions()->where('id', $requisition->id)->first();
        $totalOpenedTender = $requisition->tenders()->where('status', RequisitionStatus::OPEN)->get();
        $totalClosedTender = $requisition->tenders()->where('status', RequisitionStatus::CLOSE)->get();
        $totalCancelledTender = $requisition->tenders()->where('status', RequisitionStatus::CANCEL)->get();

        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertCount(4, $totalOpenedTender);
        $this->assertCount(3, $totalClosedTender);
        $this->assertCount(3, $totalCancelledTender);
        $this->assertCount(10, $provider->tenders);
    }

    public function testGetTotalTenderInRequisition()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $requisition = $this->getDummyOpenedRequisition($company, $branch);

        for ($i = 0; $i < 10; $i++) {
            $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);
        }

        $requisition = $company->requisitions()->where('id', $requisition->id)->first();
        $tenders = $requisition->tenders()->where('requisition_id', $requisition->id)->get();

        $this->assertCount(10, $tenders);
    }

    public function testGetTotalRequisitionByStatus()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();

        $date = Carbon::now();
        for ($i = 0; $i < 10; $i++) {
            $requisition = $company->openRequest(
                'Company Requisition',
                'This is my first requisition',
                $date,
                $date->copy()->addWeeks(1),
                $branch
            );
            switch ($i) {
                case 2:
                case 3:
                case 4:
                    $company->postRequest(
                        $requisition,
                        Carbon::now(),
                        rand(1, 100),
                        rand(1, 100),
                        RateType::HOURLY
                    );
                    break;
                case 5:
                case 6:
                case 7:
                    $company->closeRequest($requisition);
                    break;
            }
        }

        $totalOpenedRequisition =
            $company->requisitions()->where('status', RequisitionStatus::OPEN)->get();
        $totalPostedRequisition =
            $company->requisitions()->where('status', RequisitionStatus::POST)->get();
        $totalClosedRequisition =
            $company->requisitions()->where('status', RequisitionStatus::POST)->get();

        $this->assertCount(10, $company->requisitions);
        $this->assertCount(4, $totalOpenedRequisition);
        $this->assertCount(3, $totalPostedRequisition);
        $this->assertCount(3, $totalClosedRequisition);
    }

    public function testGetTotalApplicationByStatusInTender()
    {
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $members = $provider->members;
        $requisition = $this->getDummyPostedRequisition($company, $branch, $provider);
        $tender = $this->getDummyTender($requisition);

        $members->each(function ($member, $index) use ($tender, $company) {
            $applicant = $member->applyTender($tender, Carbon::now());
            $provider = $member->provider;
            switch ($index) {
                case 0:
                case 1:
                case 2:
                    $provider->acceptApplicant($applicant);
                    break;
                case 3:
                case 4:
                case 5:
                    $company->acceptApplicant($applicant);
                    break;
                case 6:
                case 7:
                    $company->rejectApplicant($applicant);
                    break;
            }
        });

        $requisition = $company->requisitions()->where('id', $requisition->id)->first();
        $tender = $requisition->tenders()->where('requisition_id', $requisition->id)->where('id', $tender->id)->first();

        $totalAcceptedByProviderApplication =
            $tender->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_PROVIDER)->get();
        $totalSubmittedApplication =
            $tender->applicants()->where('status', ApplicantStatus::SUBMITTED)->get();
        $totalAcceptedByCompanyApplication =
            $tender->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_COMPANY)->get();
        $totalRejectedByCompanyApplication =
            $tender->applicants()->where('status', ApplicantStatus::REJECTED_BY_COMPANY)->get();

        $this->assertCount(10, $tender->applicants);
        $this->assertCount(10, $provider->members);
        $this->assertCount(5, $totalAcceptedByProviderApplication);
        $this->assertCount(3, $totalAcceptedByCompanyApplication);
        $this->assertCount(2, $totalRejectedByCompanyApplication);
        $this->assertCount(0, $totalSubmittedApplication);
    }

}
