<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Config;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Orchestra\Testbench\TestCase;
use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Helpers\Enums\RateType;
use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Helpers\Enums\TenderStatus;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;

class MemberTest extends TestCase
{
    use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testApplyTender()
    {
        $member = factory(Member::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();
        $applicant = $member->applyTender($tender, Carbon::now());

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->isAutoAcceptApplicant() ?
            $this->assertEquals(ApplicantStatus::SUBMITTED, $applicant->status) :
            $this->assertEquals(ApplicantStatus::ACCEPTED_BY_PROVIDER, $applicant->status);
    }

    public function testCancelApplication()
    {
        $member = factory(Member::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();

        $applicant = $this->getApplicant($tender)->first();
        $member->cancelApplication($applicant);

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertEquals(ApplicantStatus::CANCELLED, $applicant->status);
    }

    public function testGetApplicantFromTender()
    {
        $member = factory(Member::class)->create();
        $requisition = $this->getRequisition();
        $requisition->setStatusAsPost();
        $tender = $this->getTender($requisition);
        $tender->setStatusAsOpen();
        $this->getApplicant($tender, $member)->first();

        $this->assertInstanceOf(Requisition::class, $requisition);
        $this->assertInstanceOf(Tender::class, $tender);
        $this->assertEquals(RequisitionStatus::POST, $requisition->status);
        $this->assertEquals(TenderStatus::OPEN, $tender->status);
        $this->assertInstanceOf(Applicant::class, $member->applicant($tender));
    }


    // public function testRelationShip()
    // {
    //     $company = $this->getDummyCompanyModel();
    //     $provider = $company->providers()->first();
    //     $member  = $provider->members()->first();

    //     $this->assertInstanceOf(Provider::class, $member->provider);
    //     $this->assertInstanceOf(User::class, $member->user);
    // }

    public function testGetTotalAppliedTender()
    {
        $faker = Faker::create();
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $member = $provider->members()->first();
        $requisitions = $this->getDummyPostedRequisition($company, $branch, $provider, 10);

        $requisitions->each(function ($requisition, $index) use ($member, $faker) {
            $tender = $this->getDummyTender($requisition);
            switch ($index) {
                case 0:
                case 1:
                case 2:
                case 3:
                    $member->applyTender($tender, $faker->dateTime);
                    break;
            }
        });

        $this->assertCount(4, $member->applicants);
        $this->assertCount(10, $requisitions);
    }


    public function testGetTotalApplicationByStatusTender()
    {
        $faker = Faker::create();
        $company = $this->getDummyCompanyModel();
        $branch = $company->branches()->first();
        $provider = $company->providers()->first();
        $member = $provider->members()->first();
        $requisitions = $this->getDummyPostedRequisition($company, $branch, $provider, 10);


        $requisitions->each(function ($requisition, $index) use ($member, $faker) {

            $tender = $this->getDummyTender($requisition);
            $provider = $member->provider;
            $company = $provider->company;

            Config::set('core_config.auto_accept_applicant', false);

            switch ($index) {
                case 1:
                    Config::set('core_config.auto_accept_applicant', true);
                    $member->applyTender($tender, $faker->dateTime);
                    break;
                case 2:
                case 3:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $member->cancelApplication($applicant);
                    break;
                case 4:
                case 5:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->rejectApplicant($applicant);
                    break;
                case 6:
                case 7:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    break;
                case 8:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    $company->acceptApplicant($applicant);
                    break;
                case 9:
                    $applicant = $member->applyTender($tender, $faker->dateTime);
                    $provider->acceptApplicant($applicant);
                    $company->rejectApplicant($applicant);
                    break;
            }
        });

        $totalSubmittedApplication =
            $member->applicants()->where('status', ApplicantStatus::SUBMITTED)->get();
        $totalCancelledApplication =
            $member->applicants()->where('status', ApplicantStatus::CANCELLED)->get();
        $totalAcceptedByProviderApplication =
            $member->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_PROVIDER)->get();
        $totalRejectedByProviderApplication =
            $member->applicants()->where('status', ApplicantStatus::REJECTED_BY_PROVIDER)->get();
        $totalAcceptedByCompanyApplication =
            $member->applicants()->where('status', ApplicantStatus::ACCEPTED_BY_COMPANY)->get();
        $totalRejectedByCompanyApplication =
            $member->applicants()->where('status', ApplicantStatus::REJECTED_BY_COMPANY)->get();

        $this->assertCount(9, $member->applicants);
        $this->assertCount(10, $requisitions);
        $this->assertCount(1, $totalSubmittedApplication);
        $this->assertCount(2, $totalCancelledApplication);
        $this->assertCount(2, $totalAcceptedByProviderApplication);
        $this->assertCount(2, $totalRejectedByProviderApplication);
        $this->assertCount(1, $totalAcceptedByCompanyApplication);
        $this->assertCount(1, $totalRejectedByCompanyApplication);
    }

    // public function testGetCollectionOfMatchesTender()
    // {
    //     $company = $this->getDummyCompanyModel();
    //     $branch = $company->branches()->first();
    //     $provider = $company->providers()->first();
    //     $member = $provider->members()->first();
    //     $requisitions = $this->getDummyPostedRequisition($company, $branch, $provider, 10);

    //     foreach ($requisitions as $key => $requisition) {
    //         $tender = $provider->openTender($requisition, RateType::HOURLY, rand(1, 100), RateType::HOURLY);
    //         $provider->pushNotification($tender);
    //         // $this->assertCount(10, $tender->invitations);
    //     }
    //     $this->assertCount(10, $member->tenders);
    // }
}
