<?php

namespace Tests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Config;
use Orchestra\Testbench\TestCase;
use Tests\Helpers\Traits\EnvironmentSetupHelper;
use Tests\Helpers\Traits\TestHelper;
use Tests\InheritedPayment;

class OverridePaymentTest extends TestCase
{
 	use DatabaseTransactions;
    use TestHelper;
    use EnvironmentSetupHelper;
	
	public function testExample() 
	{
		return $this->assertTrue(true);
	}
	
	// public function testCustomPayment()
	// {
	// 	Config::set('core_config.payment', \Tests\CustomPayment::class);
	// 	$tender = $this->getDummyTenderWithApplicant();
	// 	$company = $tender->provider->company;
	// 	if ($company->hasPaymentDriver()) {
	// 		$customPayment = config('core_config.payment');
	// 		$this->assertEquals($customPayment::customPayment(), 'This is custom payment');
	// 	}
	// }

	// public function testInheritedPayment()
	// {	
	// 	$inheritedPayment = InheritedPayment::inheritedPayment();
	// 	$this->assertEquals($inheritedPayment, "This is inherited payment from custom payment");
	// }
}