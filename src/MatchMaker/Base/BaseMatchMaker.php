<?php

namespace SmartUber\Core\MatchMaker\Base;

use SmartUber\Core\Helpers\Contracts\MatchMakerContract;

abstract class BaseMatchMaker implements MatchMakerContract
{
    public static function matchingProvider($company, $requisition)
    {
        return $company->providers;
    }

    public static function matchingMember($provider, $tender)
    {
        return $provider->members;
    }

    public static function getProviderMatchingResult($company, $requisition)
    {
        return self::matchingProvider($company, $requisition);
    }

    public static function getMemberMatchingResult($provider, $tender)
    {
        return self::matchingMember($provider, $tender);
    }
}
