<?php

namespace SmartUber\Core\Notification\Base;

use SmartUber\Core\Helpers\Contracts\NotificationContract;

abstract class BaseNotification implements NotificationContract
{
    protected static $title;
    protected static $message;

    public static function setTitle($string)
    {
        return self::$title = $string;
    }

    public static function getTitle()
    {
        return self::$title;
    }

    public static function setMessage($string)
    {
        return self::$message = $string;
    }

    public static function getMessage()
    {
        return self::$message;
    }

    public static function pushEmailNotification()
    {
        return self::getMessage();
    }

    public static function pushNotification()
    {
        return self::getMessage();
    }
}
