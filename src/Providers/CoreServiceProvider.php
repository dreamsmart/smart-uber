<?php

namespace SmartUber\Core\Providers;

use Illuminate\Support\ServiceProvider;
    
class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/CoreConfig.php' => config_path('core_config.php'),
            __DIR__.'/../../migrations/' => database_path('migrations'),
            __DIR__.'/../../factories/' => database_path('factories')
        ], "core-config");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/CoreConfig.php', 'core_config');
    }
}
