<?php

namespace SmartUber\Core\Helpers\Contracts;

interface NotificationContract
{
    public static function pushEmailNotification();
    public static function pushNotification();
}
