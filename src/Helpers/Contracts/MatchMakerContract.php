<?php

namespace SmartUber\Core\Helpers\Contracts;

interface MatchMakerContract
{
    public static function matchingProvider($company, $requisition);
    public static function matchingMember($provider, $tender);
    public static function getProviderMatchingResult($company, $requisition);
    public static function getMemberMatchingResult($provider, $tender);
}
