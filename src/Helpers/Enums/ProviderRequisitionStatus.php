<?php

namespace SmartUber\Core\Helpers\Enums;

use SmartUber\Core\Helpers\Enums\BaseEnum;

final class ProviderRequisitionStatus extends BaseEnum
{
    const ACCEPT = 0;
    const REJECT = 1;
    const CANCEL = 2;

    public static function getList()
    {
        return [
            self::ACCEPT,
            self::REJECT,
            self::CANCEL
        ];
    }

    public static function getString($val)
    {
        switch ($val) {
            case 0:
                return "Accept";
            case 1:
                return "Reject";
            case 2:
                return "Cancel";
        }
    }
}
