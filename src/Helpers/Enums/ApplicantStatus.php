<?php

namespace SmartUber\Core\Helpers\Enums;

use SmartUber\Core\Helpers\Enums\BaseEnum;

final class ApplicantStatus extends BaseEnum
{
    const SUBMITTED = 0;
    const CANCELLED = 1;
    const ACCEPTED_BY_COMPANY = 2;
    const REJECTED_BY_COMPANY = 3;
    const ACCEPTED_BY_PROVIDER = 4;
    const REJECTED_BY_PROVIDER = 5;

    public static function getList()
    {
        return [
            self::SUBMITTED,
            self::CANCELLED,
            self::ACCEPTED_BY_COMPANY,
            self::REJECTED_BY_COMPANY,
            self::ACCEPTED_BY_PROVIDER,
            self::REJECTED_BY_PROVIDER
        ];
    }

    public static function getString($val)
    {
        switch ($val) {
            case 0:
                return "Submitted";
            case 1:
                return "Cancelled";
            case 2:
                return "Accepted by company";
            case 3:
                return "Rejected by company";
            case 4:
                return "Accepted by provider";
            case 5:
                return "Rejected by provider";
        }
    }
}
