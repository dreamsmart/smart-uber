<?php

namespace SmartUber\Core\Helpers\Enums;

use SmartUber\Core\Helpers\Enums\BaseEnum;

final class RequisitionStatus extends BaseEnum
{
    const OPEN = 0;
    const POST = 1;
    const CANCEL = 2;
    const CLOSE = 3;
    const COMPLETED = 4;
    const CONFIRMED = 5;

    public static function getList()
    {
        return [
            self::OPEN,
            self::POST,
            self::CANCEL,
            self::CLOSE,
            self::COMPLETED,
            self::CONFIRMED
        ];
    }

    public static function getString($val)
    {
        switch ($val) {
            case 0:
                return "Open";
            case 1:
                return "Post";
            case 2:
                return "Cancel";
            case 3:
                return "Close";
            case 4:
                return "Completed";
            case 5:
                return "Confirmed";
        }
    }
}
