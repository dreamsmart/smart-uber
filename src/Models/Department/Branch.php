<?php

namespace SmartUber\Core\Models\Department;

use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Pivot\BranchUser;
use SmartUber\Core\Models\Requisition\Requisition;

class Branch extends BaseModel
{
    public $timestamps = true;
    protected $table = 'branches';

    protected $fillable = [
        "name", "code", "address",
        "phone_number"
    ];
    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, "company_id");
    }

    public function requisitions()
    {
        return $this->hasMany(Requisition::class, "branch_id");
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'branch_users', 'branch_id', 'user_id')
            ->using(BranchUser::class)
            ->withTimestamps();
    }

    public function openRequest($subject, $startDate, $endDate, $description = null)
    {
        $requisition = new Requisition();
        $company = $this->company;
        $requisition->company()->associate($company);
        $requisition->branch()->associate($this);
        $requisition->subject = $subject;
        $requisition->description = $description;
        $requisition->start_date = $startDate;
        $requisition->end_date = $endDate;
        $requisition->status = RequisitionStatus::OPEN;
        $requisition->save();
        return $requisition;
    }
}
