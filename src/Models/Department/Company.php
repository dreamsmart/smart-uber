<?php

namespace SmartUber\Core\Models\Department;

use DB;
use SmartUber\Core\Exceptions\UnknownInterfaceException;
use SmartUber\Core\Exceptions\UnknownModelException;
use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Pivot\CompanyUser;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\Requisition;

class Company extends BaseModel
{
    public $timestamps = true;
    protected $table = 'companies';

    protected $fillable = [
        "name", "code", "address",
        "phone_number"
    ];
    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function branches()
    {
        return $this->hasMany(Branch::class, "company_id");
    }

    public function requisitions()
    {
        return $this->hasMany(Requisition::class, "company_id");
    }

    public function providers()
    {
        return $this->hasMany(Provider::class, "company_id");
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'company_users', 'company_id', 'user_id')
            ->using(CompanyUser::class)
            ->withTimestamps();
    }

    public function openRequest($subject, $description, $startDate, $endDate, $branch = null)
    {
        $requisition = new Requisition();
        $requisition->branch()->associate($branch);
        $requisition->company()->associate($this);
        $requisition->subject = $subject;
        $requisition->description = $description;
        $requisition->start_date = $startDate;
        $requisition->end_date = $endDate;
        $requisition->status = RequisitionStatus::OPEN;
        $requisition->save();
        return $requisition;
    }

    public function postRequest(
        $requisition,
        $recruitmentDate = null,
        $minPrice = null,
        $maxPrice = null,
        $rateType = null
    ) {
        $requisition->min_price = $minPrice;
        $requisition->max_price = $maxPrice;
        $requisition->rate_type = $rateType;
        $requisition->recruitment_date = $recruitmentDate;
        $requisition->status = RequisitionStatus::POST;
        $requisition->save();
        return $requisition;
    }

    public function closeRequest($requisition)
    {
        return $requisition->setStatusAsClose();
    }

    public function cancelRequest()
    {
        return $requisition->setStatusAsCancel();
    }

    public function acceptApplicant($applicant)
    {
        return $applicant->setStatusAsAccepted();
    }

    public function rejectApplicant($applicant)
    {
        return $applicant->setStatusAsRejected();
    }

    public function onClosing($requisition)
    {
        return $requisition->setStatusAsClose();
    }

    public function makePayment($tender, $startDate, $endDate)
    {
        if (!$this->hasPaymentDriver()) {
            return false;
        }

        $payment = config("core_config.payment");
        return $payment::calculatePayment($tender, $startDate, $endDate);
    }

    protected function hasPaymentDriver()
    {
        $payment = config("core_config.payment");
        if (! class_exists($payment)) {
            throw new UnknownModelException($payment. ' class not found');
        }

        if (!
            in_array(
                "SmartUber\Payment\Helpers\Contracts\PaymentContract",
                class_implements($payment)
            )
        ) {
            throw new UnknownInterfaceException(
                $payment. ' must implements SmartUber\Payment\Helpers\Contracts\PaymentContract'
            );
        }

        return true;
    }
}
