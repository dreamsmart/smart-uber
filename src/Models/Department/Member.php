<?php

namespace SmartUber\Core\Models\Department;

use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;
use SmartUber\Core\Models\Pivot\MemberUser;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\Invitation;
use SmartUber\Core\Models\Requisition\Tender;

class Member extends BaseModel
{
    public $timestamps = true;
    protected $fillable = [
        "name", "code", "address",
        "phone_number"
    ];
    protected $table = 'members';

    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, "member_users", 'member_id', 'user_id')
            ->using(MemberUser::class)
            ->withTimestamps();
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, "provider_id");
    }

    public function applicants()
    {
        return $this->hasMany(Applicant::class, "member_id");
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class, "member_id");
    }

    public function tenders()
    {
        return $this->belongsToMany(Tender::class, 'invitations');
    }

    public function applyTender($tender, $applyDate)
    {
        $applicant = new Applicant();
        $applicant->tender()->associate($tender);
        $applicant->member()->associate($this);
        $applicant->action_date = $applyDate;
        $applicant->save();
        $this->isAutoAcceptApplicant() ?
            $applicant->setStatusAsSubmitted() : $applicant->setStatusAsAcceptedByProvider();
        return $applicant;
    }

    public function applicant($tender)
    {
        return $this->applicants()->where([
            'member_id' => $this->id,
            'tender_id' => $tender->id
        ])->first();
    }

    // public function getCollectionOfMatchesTender()
    // {
    //     $matchMaker = $this->generateMatchMaker();
    // }

    // public function generateMatchMaker()
    // {
    //     $matchMaker = config('core_config.match_maker');
    //     if (class_exists($matchMaker)) {
    //         return false;
    //     }

    //     // if (new $matchMaker instanceof MatchMaker) {
    //     //     return false;
    //     // }
    //     return $matchMaker::getProviderMatchingResult($this);
    // }

    public function cancelApplication($applicant)
    {
        return $applicant->setStatusAsCancelled();
    }

    public function salary()
    {
        if ($this->isClassSalaryExists()) {
            $salary = config('payment_config.salary');
            return $this->hasOne($salary);
        }
        return false;
    }
}
