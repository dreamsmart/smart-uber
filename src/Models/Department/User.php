<?php

namespace SmartUber\Core\Models\Department;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Pivot\BranchUser;
use SmartUber\Core\Models\Pivot\CompanyUser;
use SmartUber\Core\Models\Pivot\MemberUser;
use SmartUber\Core\Models\Pivot\ProviderUser;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        "username",
        'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];
    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    protected $table = 'users';

    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'provider_users', 'user_id', 'provider_id')
            ->using(ProviderUser::class)
            ->withTimestamps();
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'member_users', 'user_id', 'member_id')
            ->using(MemberUser::class)
            ->withTimestamps();
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_users', 'user_id', 'company_id')
            ->using(CompanyUser::class)
            ->withTimestamps();
    }

    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_users', 'user_id', 'branch_id')
            ->using(BranchUser::class)
            ->withTimestamps();
    }

    public function member($provider)
    {
        return $this->members()->where([
            "provider_id" => $provider->id,
            "user_id" => $this->id
        ])->first();
    }

    public function provider($company)
    {
        return $this->providers()->where([
            "company_id" => $company->id,
            "user_id" => $this->id
        ])->first();
    }

    public function scopeAllProviderUser($query)
    {
        $providerUserArr = Provider::join(
            'provider_users',
            'provider_users.provider_id',
            'providers.id'
        )->pluck('provider_users.user_id');

        return $query->whereIn('id', $providerUserArr);
    }

    public function scopeAllCompanyUser($query)
    {
        $companyUserArr = Company::join(
            'company_users',
            'company_users.company_id',
            'companies.id'
        )->pluck('company_users.user_id');

        return $query->whereIn('id', $companyUserArr);
    }

    public function scopeAllBranchUser($query)
    {
        $branchUserArr = Branch::join(
            'branch_users',
            'branch_users.branch_id',
            'branches.id'
        )->pluck('branch_users.user_id');

        return $query->whereIn('id', $branchUserArr);
    }

    public function scopeAllMemberUser($query)
    {
        $memberUserArr = Company::join(
            'member_users',
            'member_users.member_id',
            'members.id'
        )->pluck('member_users.user_id');

        return $query->whereIn('id', $memberUserArr);
    }
}
