<?php

namespace SmartUber\Core\Models\Department;

use SmartUber\Core\Helpers\Enums\TenderStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\User;
use SmartUber\Core\Models\Pivot\ProviderUser;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;

class Provider extends BaseModel
{
    public $timestamps = true;
    protected $fillable = [
        "name", "code", "address",
        "phone_number"
    ];
    protected $table = 'providers';

    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, "company_id");
    }

    public function tenders()
    {
        return $this->hasMany(Tender::class, "provider_id");
    }

    public function providerRequisitions()
    {
        return $this->hasMany(ProviderRequisition::class, "provider_id");
    }

    public function users()
    {
        return $this->belongsToMany(User::class, "provider_users", 'provider_id', 'user_id')
            ->using(ProviderUser::class)
            ->withTimestamps();
    }

    public function members()
    {
        return $this->hasMany(Member::class);
    }

    public function commission()
    {
        if ($this->isClassCommissionExists()) {
            $commission = config('payment_config.commission');
            return $this->hasOne($commission);
        }
        return false;
    }

    public function requisitions()
    {
        return $this->belongsToMany(Requisition::class, 'provider_requisitions');
    }

    public function acceptCompanyRequisition(ProviderRequisition $providerRequisition)
    {
        return $providerRequisition->setStatusAsAccept();
    }

    public function rejectCompanyRequisition(ProviderRequisition $providerRequisition)
    {
        return $providerRequisition->setStatusAsReject();
    }

    public function cancelCompanyRequisition(ProviderRequisition $providerRequisition)
    {
        return $providerRequisition->setStatusAsCancel();
    }

    public function openTender(
        $requisition,
        $rateType = null,
        $markupPrice = null,
        $markupRateType = null
    ) {
        $tender = new Tender();
        $tender->provider()->associate($this);
        $tender->requisition()->associate($requisition);
        $tender->subject = $requisition->subject;
        $tender->description = $requisition->description;
        $tender->start_date = $requisition->start_date;
        $tender->end_date = $requisition->end_date;
        $tender->rate_type = $rateType;
        $tender->markup_price = $markupPrice;
        $tender->markup_rate_type = $markupRateType;
        $tender->status = TenderStatus::OPEN;
        $tender->save();

        if ($this->isAutoPushNotification()) {
            $this->pushNotification($tender);
        }
        return $tender;
    }

    public function closeTender($tender)
    {
        return $tender->setStatusAsClose();
    }

    public function cancelTender($tender)
    {
        return $tender->setStatusAsCancel();
    }

    public function acceptApplicant($applicant)
    {
        if (!$this->isAutoAcceptApplicant()) {
            $applicant->setStatusAsAcceptedByProvider();
        }
        return $applicant;
    }

    public function rejectApplicant($applicant)
    {
        if (!$this->isAutoAcceptApplicant()) {
            $applicant->setStatusAsRejectedByProvider();
        }
        return $applicant;
    }

    public function isInternal()
    {
        return $provider->company->isEmpty();
    }

    public function isAutoPushNotification()
    {
        return config('core_config.auto_push_notification');
    }

    private function getInstanceOfMatchMaker()
    {
        $matchMaker = config('core_config.match_maker');

        if (! class_exists($matchMaker)) {
            return false;
        }

        // if (! new $matchMaker instanceof MatchMaker) {
        //     return false;
        // }
        return $matchMaker;
    }

    private function getInstanceOfNotification()
    {
        $notification = config('core_config.notification');

        if (! class_exists($notification)) {
            return false;
        }

        // if (! new $notification instanceof Notification) {
        //     return false;
        // }
        return $notification;
    }

    private function generateInvitation($tender)
    {
        $matchMaker = $this->getInstanceOfMatchMaker();
        return $matchMaker::getMemberMatchingResult($this, $tender);
    }

    public function pushNotification($tender, $title = 'Notification', $message = null)
    {
        $invitations = $this->generateInvitation($tender);
        $notification = $this->getInstanceOfNotification();
        $notification::setTitle($title);
        $notification::setMessage($message);
        return $notification::pushNotification();
    }
}
