<?php

namespace SmartUber\Core\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CompanyUser extends Pivot
{
    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'company_users';

    protected $guarded = [
        "created_at",
        "updated_at",
        "user_id",
        "company_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];
}
