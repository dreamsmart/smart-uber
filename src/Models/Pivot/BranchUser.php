<?php

namespace SmartUber\Core\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BranchUser extends Pivot
{
    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'branch_users';

    protected $guarded = [
        "created_at",
        "updated_at",
        "user_id",
        "branch_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];
}
