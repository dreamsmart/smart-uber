<?php

namespace SmartUber\Core\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MemberUser extends Pivot
{
    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'member_users';

    protected $guarded = [
        "created_at",
        "updated_at",
        "user_id",
        "member_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];
}
