<?php

namespace SmartUber\Core\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProviderUser extends Pivot
{
    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'provider_users';

    protected $guarded = [
        "created_at",
        "updated_at",
        "user_id",
        "provider_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];
}
