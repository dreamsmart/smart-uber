<?php

namespace SmartUber\Core\Models\Base;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected function isAutoAcceptApplicant()
    {
        return config('core_config.auto_accept_applicant');
    }
}
