<?php

namespace SmartUber\Core\Models\Tag;

use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;

class Tag extends BaseModel
{
    public $timestamps = true;
    protected $table = 'tags';

    protected $fillable = [
        "name"
    ];
    protected $guarded = [
        "created_at",
        "updated_at"
    ];
    protected $dates = [
        "created_at",
        "updated_at"
    ];

    public function tenders()
    {
        return $this->morphedByMany(Tender::class, 'taggable');
    }

    public function requisitions()
    {
        return $this->morphedByMany(Requisition::class, 'taggable');
    }
}
