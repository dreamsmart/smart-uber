<?php

namespace SmartUber\Core\Models\Requisition;

use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Requisition\Tender;

class Invitation extends BaseModel
{
    public $timestamps = true;
    protected $table = 'invitations';

    protected $fillable = [
        "status",
        "action_date"
    ];
    protected $guarded = [
        "created_at",
        "updated_at",
        "tender_id",
        "member_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "action_date"
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, "member_id");
    }

    public function tender()
    {
        return $this->belongsTo(Tender::class, "tender_id");
    }
}
