<?php

namespace SmartUber\Core\Models\Requisition;

use SmartUber\Core\Helpers\Enums\TenderStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\Invitation;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Tag\Tag;

class Tender extends BaseModel
{
    public $timestamps = true;
    protected $table = 'tenders';

    protected $fillable = [
        "source",
        "subject",
        "description",
        "status",
        "start_date",
        "end_date",
        "rate_type",
        "markup_price",
        "markup_rate_type",
        "submitted_at",
    ];
    protected $guarded = [
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "submitted_at",
        "provider_id",
        "requisition_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "submitted_at",
        "start_date",
        "end_date"
    ];

    public function tags()
    {
        return $this->morphToMany(Tag::class, "taggable");
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, "provider_id");
    }

    public function applicants()
    {
        return $this->hasMany(Applicant::class, "tender_id");
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class, "tender_id");
    }

    public function requisition()
    {
        return $this->belongsTo(Requisition::class, "requisition_id");
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'invitations');
    }

    public function salary()
    {
        if ($this->isClassSalaryExists()) {
            $salary = config('payment_config.salary');
            return $this->hasOne($salary);
        }
        return false;
    }

    public function setStatusAsClose()
    {
        $this->status = TenderStatus::CLOSE;
        $this->save();
        return $this;
    }

    public function setStatusAsOpen()
    {
        $this->status = TenderStatus::OPEN;
        $this->save();
        return $this;
    }

    public function setStatusAsPost()
    {
        $this->status = TenderStatus::POST;
        $this->save();
        return $this;
    }

    public function setStatusAsCancel()
    {
        $this->status = TenderStatus::CANCEL;
        $this->save();
        return $this;
    }
}
