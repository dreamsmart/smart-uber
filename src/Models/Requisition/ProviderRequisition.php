<?php

namespace SmartUber\Core\Models\Requisition;

use SmartUber\Core\Helpers\Enums\ProviderRequisitionStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Requisition\Requisition;

class ProviderRequisition extends BaseModel
{
    public $timestamps = true;
    protected $table = 'provider_requisitions';

    protected $fillable = [
        "status",
        "action_date"
    ];
    protected $guarded = [
        "created_at",
        "updated_at",
        "created_by",
        "requisition_id",
        "provider_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "action_date"
    ];

    public function requisition()
    {
        return $this->belongsTo(Requisition::class, "requisition_id");
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class, "provider_id");
    }

    public function setStatusAsAccept()
    {
        $this->status = ProviderRequisitionStatus::ACCEPT;
        $this->save();
        return $this;
    }

    public function setStatusAsReject()
    {
        $this->status = ProviderRequisitionStatus::REJECT;
        $this->save();
        return $this;
    }

    public function setStatusAsCancel()
    {
        $this->status = ProviderRequisitionStatus::CANCEL;
        $this->save();
        return $this;
    }
}
