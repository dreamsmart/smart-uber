<?php

namespace SmartUber\Core\Models\Requisition;

use SmartUber\Core\Helpers\Enums\RequisitionStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Tender;
use SmartUber\Core\Models\Tag\Tag;

class Requisition extends BaseModel
{
    public $timestamps = true;
    protected $table = 'requisitions';

    protected $fillable = [
        "subject",
        "description",
        "status",
        "recruitment_date",
        "start_date",
        "end_date",
        "min_price",
        "max_price",
        "rate_type"
    ];
    protected $guarded = [
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        "submitted_at",
        "company_id",
        "branch_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "submitted_at",
        "recruitment_date",
        "start_date",
        "end_date"
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, "company_id");
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, "branch_id");
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, "taggable");
    }

    public function providerRequisitions()
    {
        return $this->hasMany(ProviderRequisition::class, "requisition_id");
    }

    public function tenders()
    {
        return $this->hasMany(Tender::class, "requisition_id");
    }

    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'provider_requisitions');
    }

    public function setStatusAsClose()
    {
        $this->status = RequisitionStatus::CLOSE;
        $this->save();
        return $this;
    }

    public function setStatusAsOpen()
    {
        $this->status = RequisitionStatus::OPEN;
        $this->save();
        return $this;
    }

    public function setStatusAsPost()
    {
        $this->status = RequisitionStatus::POST;
        $this->save();
        return $this;
    }

    public function setStatusAsCancel()
    {
        $this->status = RequisitionStatus::CANCEL;
        $this->save();
        return $this;
    }
}
