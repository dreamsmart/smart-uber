<?php

namespace SmartUber\Core\Models\Requisition;

use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Models\Base\BaseModel;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Requisition\Tender;

class Applicant extends BaseModel
{
    public $timestamps = true;
    protected $table = 'applicants';

    protected $fillable = [
        "status",
        "action_date"
    ];
    protected $guarded = [
        "created_at",
        "updated_at",
        "tender_id",
        "member_id"
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "action_date"
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, "member_id");
    }

    public function tender()
    {
        return $this->belongsTo(Tender::class, "tender_id");
    }

    public function setStatusAsSubmitted()
    {
        $this->status = ApplicantStatus::SUBMITTED;
        $this->save();
        return $this;
    }

    public function setStatusAsCancelled()
    {
        $this->status = ApplicantStatus::CANCELLED;
        $this->save();
        return $this;
    }

    public function setStatusAsAccepted()
    {
        $this->status = ApplicantStatus::ACCEPTED_BY_COMPANY;
        $this->save();
        return $this;
    }

    public function setStatusAsRejected()
    {
        $this->status = ApplicantStatus::REJECTED_BY_COMPANY;
        $this->save();
        return $this;
    }

    public function setStatusAsAcceptedByProvider()
    {
        $this->status = ApplicantStatus::ACCEPTED_BY_PROVIDER;
        $this->save();
        return $this;
    }

    public function setStatusAsRejectedByProvider()
    {
        $this->status = ApplicantStatus::REJECTED_BY_PROVIDER;
        $this->save();
        return $this;
    }
}
