<?php

use SmartUber\Core\Helpers\Enums\ApplicantStatus;
use SmartUber\Core\Helpers\Enums\ProviderRequisitionStatus;
use SmartUber\Core\Helpers\Enums\RateType;
use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Requisition\Applicant;
use SmartUber\Core\Models\Requisition\ProviderRequisition;
use SmartUber\Core\Models\Requisition\Requisition;
use SmartUber\Core\Models\Requisition\Tender;

$factory->define(Requisition::class, function (Faker\Generator $faker) {
    return [
        'subject' => $faker->sentence(5),
        'description' => $faker->text(20),
        'status' => null,
        "branch_id" => function () {
            return factory(Branch::class)->create()->id;
        },
        "company_id" => function () {
            return factory(Company::class)->create()->id;
        },
        "recruitment_date" => $faker->dateTime,
        "start_date" => $faker->dateTime,
        "end_date" => $faker->dateTime,
        "min_price" => null,
        "max_price" => null,
        "rate_type" => null,
        "submitted_at" => $faker->dateTime
    ];
});

$factory->define(Tender::class, function (Faker\Generator $faker) {
    return [
        "provider_id" => function () {
            return factory(Provider::class)->create()->id;
        },
        "requisition_id" => function () {
            return factory(Requisition::class)->create()->id;
        },
        'subject' => $faker->sentence(5),
        'description' => $faker->text(20),
        'status' => null,
        "start_date" => $faker->dateTime,
        "end_date" => $faker->dateTime,
        "markup_price" => rand(1, 100),
        "markup_rate_type" => RateType::HOURLY,
        "rate_type" => null,
        "submitted_at" => $faker->dateTime
    ];
});
$factory->define(Applicant::class, function (Faker\Generator $faker) {
    return [
        "status" => ApplicantStatus::SUBMITTED,
        "tender_id" => function () {
            return factory(Tender::class)->create()->id;
        },
        "member_id" => function() {
            return factory(Member::class)->create()->id;
        },
        "action_date" => $faker->dateTime
    ];
});

$factory->define(ProviderRequisition::class, function (Faker\Generator $faker) {
    return [
        "status" => ProviderRequisitionStatus::ACCEPT,
        "requisition_id" => function () {
            return factory(Requisition::class)->create()->id;
        },
        "provider_id" => function() {
            return factory(Provider::class)->create()->id;
        },
        "action_date" => $faker->dateTime
    ];
});