<?php

use SmartUber\Core\Models\Department\Branch;
use SmartUber\Core\Models\Department\Company;
use SmartUber\Core\Models\Department\Member;
use SmartUber\Core\Models\Department\Provider;
use SmartUber\Core\Models\Department\User;

/*
|--------------------------------------------------------------------------
| Department Models Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your Department Models factories. Department Models factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Company::class, function (Faker\Generator $faker) {
    return [
        "code" => $faker->numberBetween(1, 200),
        "name" => $faker->name,
        "phone_number" => $faker->numberBetween(1, 200),
        "address" => $faker->address
    ];
});

$factory->define(Branch::class, function (Faker\Generator $faker) {
    return [
        "code" => $faker->numberBetween(1, 200),
        "name" => $faker->name,
        "phone_number" => $faker->numberBetween(1, 200),
        "address" => $faker->address,
        "company_id" => function () {
            return factory(Company::class)->create()->id;
        }
    ];
});

$factory->define(Member::class, function (Faker\Generator $faker) {
    return [
        "code" => $faker->numberBetween(1, 200),
        "name" => $faker->name,
        "phone_number" => $faker->numberBetween(1, 200),
        "address" => $faker->address,
        "provider_id" => function () {
            return factory(Provider::class)->create()->id;
        }
    ];
});

$factory->define(Provider::class, function (Faker\Generator $faker) {
    return [
        "code" => $faker->numberBetween(1, 200),
        "name" => $faker->name,
        "phone_number" => $faker->numberBetween(1, 200),
        "address" => $faker->address,
        "company_id" => function () {
            return factory(Company::class)->create()->id;
        }
    ];
});
