<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Payment
    |--------------------------------------------------------------------------
    |
    | 
    */
    "payment" => SmartUber\Payment\Models\Payment::class,
    /*
    |--------------------------------------------------------------------------
    | Match maker
    |--------------------------------------------------------------------------
    |
    | 
    */
    "match_maker" => SmartUber\Core\MatchMaker\MatchMaker::class,
    /*
    |--------------------------------------------------------------------------
    | Notification
    |--------------------------------------------------------------------------
    |
    | 
    */
    "notification" => SmartUber\Core\Notification\Notification::class,
    "auto_accept_applicant" => false,
    "auto_push_notification" => false
];
