<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')
                  ->references('id')->on('companies')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->integer('branch_id')->nullable()->unsigned();
            $table->foreign('branch_id')
                  ->references('id')->on('branches')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->string('subject');
            $table->text('description', 2000)->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('recruitment_date')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->decimal('min_price', 24, 2)->nullable();
            $table->decimal('max_price', 24, 2)->nullable();
            $table->integer('rate_type')->nullable();
            $table->dateTime('submitted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitions');
    }
}
