<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')
                  ->references('id')->on('providers')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->integer('requisition_id')->unsigned();
            $table->foreign('requisition_id')
                  ->references('id')->on('requisitions')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->string('subject');
            $table->text('description', 2000)->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('rate_type')->nullable();
            $table->decimal('markup_price', 24, 2)->nullable();
            $table->decimal('markup_rate_type', 24, 2)->nullable();
            $table->dateTime('submitted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders');
    }
}
