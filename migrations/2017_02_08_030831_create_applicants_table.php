<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tender_id')->unsigned();
            $table->foreign('tender_id')
                  ->references('id')->on('tenders')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')
                  ->references('id')->on('members')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->integer('status')->nullable();
            $table->dateTime('action_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
