<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_requisitions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('requisition_id')->unsigned();
            $table->foreign('requisition_id')
                  ->references('id')->on('requisitions')
                  ->onUpdate('no action')
                  ->onDelete('no action');
                  
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')
                  ->references('id')->on('providers')
                  ->onUpdate('no action')
                  ->onDelete('no action');

            $table->integer('status')->nullable();
            $table->dateTime('action_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_requisitions');
    }
}
